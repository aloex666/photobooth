import RPi.GPIO as GPIO
import sys, os, time
import Modules.Config as Config
import Modules.Task as Task
import Modules.Photobooth as Photobooth
import Modules.Printer as Printer

GPIO.setmode(GPIO.BCM)
GPIO.setwarnings(False)

config = Config.get()

startButton = config['GPIO']['button']['start']
startButtonLed = config['GPIO']['led']['start']
printLed = config['GPIO']['led']['printing']
poseLed = config['GPIO']['led']['pose']

GPIO.setup(startButton, GPIO.IN)
GPIO.setup(printLed, GPIO.OUT)
GPIO.setup(startButtonLed, GPIO.OUT)
GPIO.setup(poseLed, GPIO.OUT)

GPIO.output(startButtonLed, False)
GPIO.output(printLed, False)
GPIO.output(poseLed, False)

processLock = False

try:
    print('Activated Photobooth...')
    Photobooth.cleanupSessionImages()
    Printer.cancelJobs()
    Printer.restart()
    Photobooth.preview()

    GPIO.output(startButtonLed, True)
    GPIO.output(printLed, False)
    GPIO.output(poseLed, False)

    print('Ready to start...')

    while True:
        if GPIO.input(startButton) and processLock is False:
            processLock = True

            takenPictures = 0
            while takenPictures < 4:
              Photobooth.pose()

              try:
                  Photobooth.takePicture()
                  takenPictures += 1
              except RuntimeError as e:
                  print e.message

              GPIO.output(poseLed, False)
              time.sleep(0.5)

            GPIO.output(printLed, True)

            montageName = config['paths']['images']['archiv']
            montageName+= config['image']['prefix']
            montageName+= time.strftime('%d-%m_%H-%M-%S')
            montageName+= config['image']['suffix']

            Photobooth.assemble(montageName)
            Photobooth.preview(montageName)

            if os.path.isfile(montageName):
                if config['printer']['enabled'] == True:
                    Printer.printImage(montageName)
                    time.sleep(10.0)
                else:
                    print('Printing disabled.')
            else:
                print('Montage not found. Were not able to print.')

            Photobooth.cleanupSessionImages()

            print('Ready to start...')
            processLock = False
            GPIO.output(startButtonLed, True)
            GPIO.output(printLed, False)
            GPIO.output(poseLed, False)

except KeyboardInterrupt as e:
    print('Application stopped')
# except:
#     processLock = False
#     print('Application stopped')
finally:
    GPIO.cleanup()
