import RPi.GPIO as GPIO
import time

import Modules.Config as Config
import Modules.Task as Task

config = Config.get()

startButtonLed = config['GPIO']['led']['start']
poseLed = config['GPIO']['led']['pose']

mediaPath = config['paths']['scripts']['base'] + 'Media/'
imageSessionPath = config['paths']['images']['session']
imageArchivPath = config['paths']['images']['archiv']
imageOriginalsPath = config['paths']['images']['originals']
imagePrefix = config['image']['prefix']
imageSuffix = config['image']['suffix']

def cleanupSessionImages():
    Task.run(
        'Cleanup old session images',
        'find '+imageSessionPath+' -type f -name "*' + imageSuffix + '" -delete'
    )

def pose():
    print('Pose!')
    GPIO.output(startButtonLed, False)
    GPIO.output(poseLed, True)
    for i in range(5):
      GPIO.output(poseLed, False)
      time.sleep(0.4)
      GPIO.output(poseLed, True)
      time.sleep(0.4)
    for j in range(5):
      GPIO.output(poseLed, False)
      time.sleep(0.1)
      GPIO.output(poseLed, True)
      time.sleep(0.1)
    GPIO.output(poseLed, False)

def takePicture():
    fileName  = imageSessionPath + imagePrefix + time.strftime('%d-%m_%H-%M-%S')+ imageSuffix

    Task.run(
        'Capture ' + fileName,
        'sudo gphoto2 --capture-image-and-download --filename "' + fileName + '"'
    )

def preview(montageName = ''):
    if montageName != '':
        Task.run(
            'Show new montage ' + montageName,
            'sudo fbi -T 1 --noverbose ' + montageName
        )
    else:
        Task.run(
            'Show last taken picture',
            'cd ' + imageArchivPath + ' || exit; ' +
            'sudo fbi -T 1 --noverbose $(ls -t | head -n1)'
        )

def assemble(montageName):
    sessionImages = imageSessionPath + '*' + imageSuffix
    sessionTmpMontage= imageSessionPath + 'montageTmp' + imageSuffix
    sessionMontage= imageSessionPath + 'montage' + imageSuffix

    Task.run(
        'Resize images...',
        'mogrify -resize 968x648 ' + sessionImages
    )

    Task.run(
        'Combine images...',
        'montage ' + sessionImages + ' -tile 2x2 -geometry +10+10 ' + sessionTmpMontage + ';' +
        'composite -gravity center ' + mediaPath + 'logo.png ' + sessionTmpMontage + ' ' + sessionMontage + ';' +
        'cp ' + sessionMontage + ' ' + montageName
    )

    Task.run(
        'Cleanup...',
        'rm ' + sessionTmpMontage + ' && ' +
        'rm ' + sessionMontage + ' && ' +
        'cp ' + sessionImages + ' ' + imageOriginalsPath + ' && ' +
        'rm ' + sessionImages
    )
