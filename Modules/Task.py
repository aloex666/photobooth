import subprocess
import Modules.Config as Config

def run(title, shellCommand):
    try:
        print(title)
        gpout = subprocess.check_output(shellCommand, stderr=subprocess.STDOUT, shell=True)
    except subprocess.CalledProcessError as e:
        raise RuntimeError('Command \'{}\' return with error (code {}): {}'.format(e.cmd, e.returncode, e.output))
    except:
        raise RuntimeError(title + ' wasn\'t successfully')
