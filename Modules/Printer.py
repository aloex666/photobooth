
import time, cups, subprocess
import Modules.Config as Config
import Modules.Task as Task

config = Config.get()
printerName = config['printer']['name']
printerMaxRetries = config['printer']['maxRetries']
cups.setUser(config['system']['user'])

def printImage(fileName):
    connection = cups.Connection()
    printers = connection.getPrinters()
    #printerName = printers.keys()[0] => this is the more flexible way

    retries = 1
    while len(connection.getJobs()) > 0 and retries <= printerMaxRetries:
        print("There are still " + str(len(connection.getJobs())) + " jobs in the queue.")
        resume()
        retries = retries + 1
        time.sleep(5.0)

    print('Print ' + fileName)
    printId = connection.printFile(printerName, fileName, title='Wedding print', options={})

    completed = False
    retries = 1
    while completed == False and retries <= printerMaxRetries:
        state = connection.getJobAttributes(printId)["job-state"]

        if state == 5:
            #IPP_JOB_PROCESSING
            print('Waiting for printer')
            retries = retries + 1
        elif state == 9:
            #IPP_JOB_COMPLETED
            completed = True
            print("Printing done. Please get your picture.")
        else:
            print('Unknown state: ' + state)
            retries = retries + 1
        time.sleep(2.0)

def cancelJobs():
    Task.run(
        'Cancel actions of ' + printerName,
        'sudo cancel -a -x ' + printerName
    )

def resume():
    Task.run(
        'Resume printer jobs',
        'sudo lpadmin -E -p ' + printerName
    )

def restart():
    Task.run(
        'Enable cups for ' + printerName,
        'sudo cupsenable ' + printerName
    )

    Task.run(
        'Restart cups service',
        'sudo pkill cups && sudo /etc/init.d/cups restart'
    )
