# Photobooth Controller #
- - - -
## Inspired by the following repositories ##
* https://github.com/safay/RPi_photobooth
* https://github.com/johols/RPi-photobooth

## Started from tutorial from instructables.com ##
> http://www.instructables.com/id/Raspberry-Pi-photo-booth-controller/

## Used third-party packages ##
* pycups (Python library to print files and get printer state)

## Additions ##
* Works on minimal Raspberry Pi installation without GUI
* Preview image on separate display
* Mounted external USB device as image folder
* Checking printer-job-send-status before going back to initial state
* On reset hard killall for python scripts to avoid performance issues due to stacked up processes
* Adding centered, round logo
* Reduced sleep times while printing, because after the job is send to the printer, new jobs will be queued up

## Note ##
* You still have to configure your printer via the CUPS page which is accessible by opening http://*your-pi-ip*:631/

## Future topics ##
* Switch to enable/disable printer-functionality

## UX ##
1. Startup
2. Show last taken picture
3. Click start
4. Taking 4 pictures with visual countdown
5. Assembling images and adding overlay image
6. Preview
7. Printing
