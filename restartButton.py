import RPi.GPIO as GPIO, subprocess, time
import Modules.Config as Config
import Modules.Task as Task
import Modules.Printer as Printer

GPIO.setmode(GPIO.BCM)
GPIO.setwarnings(False)

config = Config.get()
startButton = config['GPIO']['button']['start']
startButtonLed = config['GPIO']['led']['start']
printLed = config['GPIO']['led']['printing']
poseLed = config['GPIO']['led']['pose']
resetButton = config['GPIO']['button']['reset']
resetStatusLed = config['GPIO']['led']['reset']

GPIO.setup(startButton, GPIO.IN)
GPIO.setup(printLed, GPIO.OUT)
GPIO.setup(startButtonLed, GPIO.OUT)
GPIO.setup(poseLed, GPIO.OUT)
GPIO.setup(resetStatusLed, GPIO.OUT)
GPIO.setup(resetButton, GPIO.IN)

GPIO.output(resetStatusLed, False)
GPIO.output(startButtonLed, False)
GPIO.output(printLed, False)
GPIO.output(poseLed, False)

try:
    print('Activated restart button...')

    GPIO.output(resetStatusLed, 1)
    while (True):
        if GPIO.input(resetButton):
            Printer.cancelJobs()
            Printer.restart()

            # @TODO => make thread to blink continously
            GPIO.output(resetStatusLed, 0)
            time.sleep(.5)
            GPIO.output(resetStatusLed, 1)
            time.sleep(.5)
            GPIO.output(resetStatusLed, 0)
            time.sleep(.5)
            GPIO.output(resetStatusLed, 1)
            time.sleep(.5)
            GPIO.output(resetStatusLed, 0)
            GPIO.cleanup()

            Task.run(
                'Kill Python',
                'sh ' + config['paths']['scripts']['base'] + 'reset.sh'
            )
except KeyboardInterrupt as e:
    print('Application stopped')
#except:
#    print('Application stopped')
finally:
    GPIO.cleanup()
